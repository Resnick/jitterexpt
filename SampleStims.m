jitter = [0 45];

[Istim,time] = makeStim_PulseTrain;
[Istim_norm,time1] = makeStim_PulseTrain_Jittered('jitter',jitter(1),'trainDur',100e-3,'pw',400);
[Istim_jittered,time2] = makeStim_PulseTrain_Jittered('jitter',jitter(2),'trainDur',100e-3,'pw',400);

figure('position',[50,50,1000,800])
subplot(2,1,1)
plot(time1/1000,Istim_norm); xlim([2,102])
xticklabels([]); ylabel('Current (mA)'); ylim([-2.5,2.5]);
set(gca,'FontSize',18);

subplot(2,1,2)
plot(time2/1000,Istim_jittered); xlim([2,102])
xlabel('Time (ms)'); ylabel('Current (mA)'); ylim([-2.5,2.5]);
set(gca,'FontSize',18);
%%%%%%%%    Parameters to change for every subjects     %%%%%%%%%

Subject     ='';
device      ='CIC3';
processor   ='L-34';
ae          =; % active electrode
re          =-3; % return electrode
pwShort     =25;
pwLong      =400;
ipg         =8; % Inter-phase gap in microsec

folderpath  ='G:\My Drive\Rubinstein\CI Testing\';
jitter      = [0 45];
rove_range = 4;

% Levels determined via fitting interface
soft_pwShort        =;
comfort_pwShort     =;
loud_pwShort        =;

soft_pwShort_JIT    =;
comfort_pwShort_JIT =;
loud_pwShort_JIT    =;

soft_pwLong         =;
comfort_pwLong      =;
loud_pwLong         =;

soft_pwLong_JIT     =;
comfort_pwLong_JIT  =;
loud_pwLong_JIT     =;

codeDir = 'G:\My Drive\Rubinstein\CI Testing\JitterExpt\Apex_Expt_Files';
stimDir = 'G:\My Drive\Rubinstein\CI Testing\JitterExpt\Stimuli';
addpath(codeDir); addpath(stimDir);

%% Generate first pair of loudness balancing scripts
% Package expt info int structures for easy passing
ExptInfo = struct('Subject',Subject,'device',device,'processor',processor,...
    'ae',ae,'re',re,'pwShort',pwShort,'pwLong',pwLong,'ipg',ipg,'jitter',...
    jitter,'folderpath',folderpath);

clinicalLevels = struct('soft_pwShort',soft_pwShort,...
    'comfort_pwShort',comfort_pwShort,'loud_pwShort',loud_pwShort,...
    'soft_pwShort_JIT',soft_pwShort_JIT,'comfort_pwShort_JIT',...
    comfort_pwShort_JIT,'loud_pwShort_JIT',loud_pwShort_JIT,...
    'soft_pwLong',soft_pwLong,'comfort_pwLong',...
    comfort_pwLong,'loud_pwLong',loud_pwLong,...
    'soft_pwLong_JIT',soft_pwLong_JIT,'comfort_pwLong_JIT',...
    comfort_pwLong_JIT,'loud_pwLong_JIT',loud_pwLong_JIT);

Generation_ApexLoudnessPW(ExptInfo,clinicalLevels);

%% Generate scripts for loudness balancing jittere stims 
% Input data from first round of balancing
comfort_pwShort_a   = ; 
comfort_pwShort_b   = ;

comfort_pwLong_a    = ; 
comfort_pwLong_b    = ;

% Calculate balanced levels for long pulse duraction from results
target_pwShort        = comfort_pwShort;
PWcomfortDifference   = [   comfort_pwShort - comfort_pwLong_a 
                            comfort_pwShort - comfort_pwLong_b
                            comfort_pwShort_a - comfort_pwLong
                            comfort_pwShort_b - comfort_pwLong];
target_pwLong         = round(target_pwShort - mean(PWcomfortDifference));

fprintf(1,'Short Target Level: %d \t Long Target Level: %d \n',target_pwShort,target_pwLong)

Generation_ApexLoudnessJIT_Soft(ExptInfo,clinicalLevels,target_pwShort,target_pwLong,rove_range)

%% Generate experimental scripts and stimuli using target values
% Input data from second round of balancing
comfort_pwShortJIT_a    = ; 
comfort_pwShortJIT_b    = ;
comfort_pwShort_c       = ; 
comfort_pwShort_d       = ;

comfort_pwLongJIT_a     = ; 
comfort_pwLongJIT_b     = ;
comfort_pwLong_c        = ; 
comfort_pwLong_d        = ;

softBal_pwShort_a       = ;
softBal_pwShort_b       = ;
softBal_pwLong_a        = ;
softBal_pwLong_b        = ;

% Calculate balanced levels for jittered stims from results
shortJITcomfortDifference = [target_pwShort - comfort_pwShortJIT_a 
                            target_pwShort - comfort_pwShortJIT_b
                            comfort_pwShort_c - target_pwShort
                            comfort_pwShort_d - target_pwShort];
target_pwShortJIT = round(target_pwShort - mean(shortJITcomfortDifference));

longJITcomfortDifference = [  target_pwLong - comfort_pwLongJIT_a 
                            target_pwLong - comfort_pwLongJIT_b
                            comfort_pwLong_c - target_pwLong
                            comfort_pwLong_d - target_pwLong];
target_pwLongJIT = round(target_pwLong - mean(longJITcomfortDifference));

fprintf(1,'Short Jittered Target Level: %d \t Long Jittered Target Level: %d \n',target_pwShortJIT,target_pwLongJIT)

testJitters = [0 logspace(log10(7),log10(45),6)];

targetLevels = struct('target_pwShort',target_pwShort,'target_pwLong',...
    target_pwLong,'target_pwShortJIT',target_pwShortJIT,'target_pwLongJIT',...
    target_pwLongJIT);

Generation_ApexExptFiles(ExptInfo,targetLevels,testJitters);

softBalDifference =     [  softBal_pwShort_a - (target_pwShort - rove_range)
                           softBal_pwShort_b - (target_pwShort - rove_range) 
                           (target_pwLong - rove_range) - softBal_pwLong_a 
                           (target_pwLong - rove_range) - softBal_pwLong_b];
                       
rove_range_long     = round(rove_range + mean(softBalDifference));                   
fprintf(1,'Target Rove Range: %d \t Long PW Rove Range: %d \n',rove_range,rove_range_long)

ExptInfo.rove_range = rove_range;
ExptInfo.rove_range_long  = rove_range_long;
ExptInfo.period = 5000;
ExptInfo.duration = 400;

Generation_Stimuli_Expt(ExptInfo,targetLevels,testJitters);
%% Analyze Results
%% Subject 1
Nb_Blocks = ;
folderpath = sprintf('G:\\My Drive\\Lab\\CI Testing\\JitterExpt\\Apex_Expt_Files\\Subject%S\\Completed\\Testing\\',Subject);
addpath('.');

pwType = 'pwLong';
[longPossible,longCorrect]=import_testing_file(SubjectInitials,Nb_Blocks,pwType,folderpath);
pwType = 'pwShort';
[shortPossible,shortCorrect]=import_testing_file(SubjectInitials,Nb_Blocks,pwType,folderpath);

%% Plot
jitter=[7 10 15 21 31 45];

h = plot(jitter,shortCorrect./shortPossible,'-r.'); hold on;
set(h,'LineWidth',1.5,'MarkerSize',20)

h = plot(jitter,longCorrect./longPossible,'-b.'); hold on;
set(h,'LineWidth',1.5,'MarkerSize',20)
xlabel('Jitter (% of period)')
ylabel('Percent Correct')
set(gca,'Fontsize',14)
leghand = legend('25','400','Position','southeast');
set(leghand,'location','southeast')
title(leghand,{'Phase Duration','(\mus)'})

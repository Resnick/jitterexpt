function Generation_ApexExptFiles(ExptInfo,targetLevels,jitter)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Subject=ExptInfo.Subject;
device=ExptInfo.device;
processor=ExptInfo.processor;
ae=ExptInfo.ae;
pwShort=ExptInfo.pwShort;
pwLong=ExptInfo.pwLong;
folderpath=ExptInfo.folderpath;
% jitter_train = ExptInfo.jitter;

level_pwShort_jitter0 = targetLevels.target_pwShort;
level_pwShort_jitter45 = targetLevels.target_pwShortJIT;
level_pwLong_jitter0 = targetLevels.target_pwLong;
level_pwLong_jitter45 = targetLevels.target_pwLongJIT;

% level_pwShort_train=[level_pwShort_jitter0 level_pwShort_jitter45];
% level_pwLong_train=[level_pwLong_jitter0 level_pwLong_jitter45];

level_pwShort=round(interp1([0 45.001],[level_pwShort_jitter0 level_pwShort_jitter45],jitter));
level_pwLong=round(interp1([0 45.001],[level_pwLong_jitter0 level_pwLong_jitter45],jitter));

ApexScriptFolder = sprintf('%sJitterExpt\\Apex_Expt_Files\\Subject%s\\Training\\',folderpath,Subject);
if ~isfolder(ApexScriptFolder), mkdir(ApexScriptFolder); end
%%%%%%%%%%%%%%%%%%%%%%%%Generate training Apex files for Short phase%%%%%%%%%%%%%%%%%%%%��

for IDX=1:2
    
    Iter_Start=(IDX-1)*10+1;
    Iter_End=IDX*10;
    fid = fopen(sprintf('%sSubject%s_pwShort_Training%d.cst',ApexScriptFolder,Subject,IDX),'a');

stim_path=sprintf('path=%sJitterExpt\\Stimuli\\Subject%s\\Training\\pwShort\\',folderpath,Subject);

fprintf(fid, '[device]\r\n');
fprintf(fid, strcat('device=NIC2,connection=L34,implant=', device, '\r\n\r\n'));
fprintf(fid, '[help]\r\n');
fprintf(fid, 'task=Click on the irregular sound \r\n\r\n');
fprintf(fid, '[stimuli]\r\n');
fprintf(fid, '%s\r\n',stim_path );

for i=Iter_Start:Iter_End
        std=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwShort),'_jitter',num2str(0),'_level',num2str(level_pwShort_jitter0),'_Iter',num2str(i),'.xml');
        sig=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwShort),'_jitter',num2str(45),'_level',num2str(level_pwShort_jitter45),'_Iter',num2str(i),'.xml');
        fprintf(fid, strcat('standard=',std,'\r\n'));
        fprintf(fid, strcat('signal=',sig,',label=Iter',num2str(i),'\r\n'));
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'afc=1,2\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'feedback=yes\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'practice=no\r\n');
fprintf(fid, 'repetitions=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'highlight=yes\r\n\r\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

end

%%%%%%%%%%%%%%%%%%%%%%%%Generate training Apex files for Long phase%%%%%%%%%%%%%%%%%%%%��

for IDX=1:2
    
    Iter_Start=(IDX-1)*10+1;
    Iter_End=IDX*10;
        fid = fopen(sprintf('%sSubject%s_pwLong_Training%d.cst',ApexScriptFolder,Subject,IDX),'a');

stim_path=sprintf('path=%sJitterExpt\\Stimuli\\Subject%s\\Training\\pwLong\\',folderpath,Subject);

fprintf(fid, '[device]\r\n');
fprintf(fid, strcat('device=NIC2,connection=L34,implant=', device, '\r\n\r\n'));
fprintf(fid, '[help]\r\n');
fprintf(fid, 'task=Click on the irregular sound \r\n\r\n');
fprintf(fid, '[stimuli]\r\n');
fprintf(fid, '%s\r\n',stim_path );

for i=Iter_Start:Iter_End
        std=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwLong),'_jitter',num2str(0),'_level',num2str(level_pwLong_jitter0),'_Iter',num2str(i),'.xml');
        sig=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwLong),'_jitter',num2str(45),'_level',num2str(level_pwLong_jitter45),'_Iter',num2str(i),'.xml');
        fprintf(fid, strcat('standard=',std,'\r\n'));
        fprintf(fid, strcat('signal=',sig,',label=Iter',num2str(i),'\r\n'));
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'afc=1,2\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'feedback=yes\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'practice=no\r\n');
fprintf(fid, 'repetitions=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'highlight=yes\r\n\r\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

end

%%%%%%%%%%%%%%%%%%%%%%%%Generate testing Apex files for Short phase%%%%%%%%%%%%%%%%%%%%��
ApexScriptFolder = sprintf('%sJitterExpt\\Apex_Expt_Files\\Subject%s\\Testing\\',folderpath,Subject);
if ~isfolder(ApexScriptFolder), mkdir(ApexScriptFolder); end

for IDX=1:5
    
    Iter_Start=(IDX-1)*5+1;
    Iter_End=IDX*5;
    fid = fopen(sprintf('%sSubject%s_pwShort_Testing%d.cst',ApexScriptFolder,Subject,IDX),'a');

stim_path=sprintf('path=%sJitterExpt\\Stimuli\\Subject%s\\Testing\\pwShort\\',folderpath,Subject);

fprintf(fid, '[device]\r\n');
fprintf(fid, strcat('device=NIC2,connection=L34,implant=', device, '\r\n\r\n'));
fprintf(fid, '[help]\r\n');
fprintf(fid, 'task=Click on the irregular sound \r\n\r\n');
fprintf(fid, '[stimuli]\r\n');
fprintf(fid, '%s\r\n',stim_path );

for i=Iter_Start:Iter_End
    for j=2:length(jitter)       
        std=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwShort),'_jitter',num2str(jitter(1)),'_level',num2str(level_pwShort(1)),'_Iter',num2str(i),'.xml');
        sig=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwShort),'_jitter',num2str(round(jitter(j))),'_level',num2str(level_pwShort(j)),'_Iter',num2str(i),'.xml');
        fprintf(fid, strcat('standard=',std,'\r\n'));
        fprintf(fid, strcat('signal=',sig,',label=J',num2str(round(jitter(j))),'_Iter',num2str(i),'\r\n'));
    end
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'afc=1,2\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'feedback=yes\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'practice=no\r\n');
fprintf(fid, 'repetitions=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'highlight=yes\r\n\r\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

end

%%%%%%%%%%%%%%%%%%%%%%%%Generate testing Apex files for Long phase%%%%%%%%%%%%%%%%%%%%��

for IDX=1:5
    
    Iter_Start=(IDX-1)*5+1;
    Iter_End=IDX*5;
    fid = fopen(sprintf('%sSubject%s_pwLong_Testing%d.cst',ApexScriptFolder,Subject,IDX),'a');

stim_path=sprintf('path=%sJitterExpt\\Stimuli\\Subject%s\\Testing\\pwLong\\',folderpath,Subject);

fprintf(fid, '[device]\r\n');
fprintf(fid, strcat('device=NIC2,connection=L34,implant=', device, '\r\n\r\n'));
fprintf(fid, '[help]\r\n');
fprintf(fid, 'task=Click on the irregular sound \r\n\r\n');
fprintf(fid, '[stimuli]\r\n');
fprintf(fid, '%s\r\n',stim_path );

for i=Iter_Start:Iter_End
    for j=2:length(jitter)
        std=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwLong),'_jitter',num2str(jitter(1)),'_level',num2str(level_pwLong(1)),'_Iter',num2str(i),'.xml');
        sig=strcat('Subject',num2str(Subject),'_E',num2str(ae),'_pw',num2str(pwLong),'_jitter',num2str(round(jitter(j))),'_level',num2str(level_pwLong(j)),'_Iter',num2str(i),'.xml');
        fprintf(fid, strcat('standard=',std,'\r\n'));
        fprintf(fid, strcat('signal=',sig,',label=J',num2str(round(jitter(j))),'_Iter',num2str(i),'\r\n'));
    end
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'afc=1,2\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'feedback=yes\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'practice=no\r\n');
fprintf(fid, 'repetitions=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'highlight=yes\r\n\r\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

end
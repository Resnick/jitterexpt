function Generation_ApexLoudnessJIT_Soft(ExptInfo,clinicalLevels,target_pwShort,target_pwLong,rove_range)
Subject=ExptInfo.Subject;
device=ExptInfo.device;
processor=ExptInfo.processor;
ae=ExptInfo.ae;
re=ExptInfo.re;
pwShort=ExptInfo.pwShort;
pwLong=ExptInfo.pwLong;
ipg=ExptInfo.ipg;
folderpath=ExptInfo.folderpath;
jitter = ExptInfo.jitter;


soft_pwShort = clinicalLevels.soft_pwShort;
comfort_pwShort = clinicalLevels.comfort_pwShort;
loud_pwShort = clinicalLevels.loud_pwShort;

soft_pwShort_JIT = clinicalLevels.soft_pwShort_JIT;
comfort_pwShort_JIT = clinicalLevels.comfort_pwShort_JIT;
loud_pwShort_JIT = clinicalLevels.loud_pwShort_JIT;

soft_pwLong = clinicalLevels.soft_pwLong;
comfort_pwLong = clinicalLevels.comfort_pwLong;
loud_pwLong = clinicalLevels.loud_pwLong;

soft_pwLong_JIT = clinicalLevels.soft_pwLong_JIT;
comfort_pwLong_JIT = clinicalLevels.comfort_pwLong_JIT;
loud_pwLong_JIT = clinicalLevels.loud_pwLong_JIT;

ApexScriptFolder = strcat(folderpath,'JitterExpt\Apex_Expt_Files\Subject',Subject,'\\Loudness2\\');
if ~isfolder(ApexScriptFolder), mkdir(ApexScriptFolder); end
%% %%%%%%%%%%% Short-ShortJitter %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwShort),'_to_PW',num2str(pwShort),'JIT','.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\',device,filesep);

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(2),comfort_pwShort_JIT);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwShort);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwShort);
for currentLevel = loud_pwShort:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(1),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

%% %%%%%%%%%%% ShortJitter-Short %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwShort),'JIT','_to_PW',num2str(pwShort),'.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\',device,filesep);

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(1),target_pwShort);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwShort_JIT);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwShort_JIT);
for currentLevel = loud_pwShort_JIT:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(2),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

%% %%%%%%%%%%% Long-LongJitter %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwLong),'_to_PW',num2str(pwLong),'JIT','.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\',device,filesep);

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(2),comfort_pwLong_JIT);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwLong);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwLong);
for currentLevel = loud_pwLong:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(1),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

%% %%%%%%%%%%% LongJitter-Long %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwLong),'JIT','_to_PW',num2str(pwLong),'.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\',device,filesep);

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(1),target_pwLong);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwLong_JIT);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwLong_JIT);
for currentLevel = loud_pwLong_JIT:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(2),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

%% %%%%%%%%%%% Short-LongSoft %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwShort),'_to_PW',num2str(pwLong),'Soft','.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\',device,filesep);

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(1),target_pwLong-rove_range);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwShort);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwShort);
for currentLevel = loud_pwShort:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(1),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

%% %%%%%%%%%%% Long-ShortSoft %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwLong),'_to_PW',num2str(pwShort),'Soft','.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\',device,filesep);

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(1),target_pwShort-rove_range);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwLong);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwLong);
for currentLevel = loud_pwLong:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(1),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);
function Generation_ApexLoudnessPW(ExptInfo,clinicalLevels)
Subject=ExptInfo.Subject;
device=ExptInfo.device;
processor=ExptInfo.processor;
ae=ExptInfo.ae;
re=ExptInfo.re;
pwShort=ExptInfo.pwShort;
pwLong=ExptInfo.pwLong;
ipg=ExptInfo.ipg;
folderpath=ExptInfo.folderpath;
jitter = ExptInfo.jitter;


soft_pwShort = clinicalLevels.soft_pwShort;
comfort_pwShort = clinicalLevels.comfort_pwShort;
loud_pwShort = clinicalLevels.loud_pwShort;

soft_pwLong = clinicalLevels.soft_pwLong;
comfort_pwLong = clinicalLevels.comfort_pwLong;
loud_pwLong = clinicalLevels.loud_pwLong;

%%%%%%%%%%%%%    Generate  Apex Loudness Balancing files  %%%%%%%%%%%
ApexScriptFolder = strcat(folderpath,'JitterExpt\Apex_Expt_Files\Subject',Subject,'\\Loudness1\\');
if ~isfolder(ApexScriptFolder), mkdir(ApexScriptFolder); end
%% %%%%%%%%%%% Short-LongComfort %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwShort),'_to_PW',num2str(pwLong),'Comfort','.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\',device,filesep);

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(1),comfort_pwLong);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwShort);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwShort);
for currentLevel = loud_pwShort:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(1),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);

%% %%%%%%%%%%% Long-ShortComfort %%%%%%%%%%%%%%%%
fid = fopen(strcat(ApexScriptFolder,'Subject',Subject,'_LoudnessBal_PW',num2str(pwLong),'_to_PW',num2str(pwShort),'Comfort','.adj'),'a');

stim_path=strcat('path=',folderpath,'JitterExpt\Stimuli\Loudness\');

fprintf(fid,'[device]\r\n');
fprintf(fid,'device=NIC2,connection=L34,implant=%s\r\n\r\n',device);
fprintf(fid,'[help]\r\n');
fprintf(fid,'task=Adjust the second sound to make it as loud as the first. +/++/+++ makes it louder -/--/--- softer \r\n\r\n');
fprintf(fid,'[stimuli]\r\n');
fprintf(fid,'%s\r\n',stim_path );

standardFileName = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwShort,jitter(1),comfort_pwShort);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,soft_pwLong);
fprintf(fid,'standard=%s,startlabel=%d\n',standardFileName,loud_pwLong);
for currentLevel = loud_pwLong:-1:0
    signalFile = sprintf('E%d_pw%d_jitter%d_level%d.xml',ae,pwLong,jitter(1),currentLevel);
    fprintf(fid,'signal=%s,label=%d\n',signalFile,currentLevel);
end

fprintf(fid, '\r\n');
fprintf(fid, '[method]\r\n');
fprintf(fid, 'repetitions=1\r\n');
fprintf(fid, 'gap=100\r\n');
fprintf(fid, 'pause=1000\r\n');
fprintf(fid, 'intervals=2\r\n');
fprintf(fid, 'printlist=yes\r\n');
fprintf(fid, 'signalposition=2\r\n');
fprintf(fid, 'stepsizes=1,2,3\r\n\n');
fprintf(fid, '[end]\r\n\r\n' );
    
fclose(fid);
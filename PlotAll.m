%% Plot group
jitter=[7 10 15 21 31 45];

shortMean = mean(shortPerC,1);
shortSTD = std(shortPerC,[],1);
longMean = mean(longPerC,1);
longSTD = std(longPerC,[],1);
n = size(shortMean,1);

h = errorbar(jitter,shortMean,shortSTD/sqrt(n),'-r.'); hold on;
set(h,'LineWidth',1.5,'MarkerSize',20)

h = errorbar(jitter,longMean,longSTD/sqrt(n),'-b.'); hold on;
set(h,'LineWidth',1.5,'MarkerSize',20)
xlabel('Jitter (% of period)')
ylabel('Percent Correct'); ylim([0.4,1]);
set(gca,'Fontsize',14)
leghand = legend('25','400','Position','southeast');
set(leghand,'location','northwest')
title(leghand,{'Phase Duration','(\mus)'})
title('Group Data');
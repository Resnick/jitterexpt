function Generation_Stimuli_Expt(ExptInfo,targetLevels,jitter)
Subject=ExptInfo.Subject;
device=ExptInfo.device;
processor=ExptInfo.processor;
ae=ExptInfo.ae;
re=ExptInfo.re;
ipg=ExptInfo.ipg;
pwShort=ExptInfo.pwShort;
pwLong=ExptInfo.pwLong;
folderpath=ExptInfo.folderpath;
jitter_train = ExptInfo.jitter;

rove_range=ExptInfo.rove_range;  % Number of CU for roving
rove_range_long =ExptInfo.rove_range_long;

period=ExptInfo.period; % mean period in microsec
duration=ExptInfo.duration; % stim duration in ms

level_pwShort_jitter0 = targetLevels.target_pwShort;
level_pwShort_jitter45 = targetLevels.target_pwShortJIT;
level_pwLong_jitter0 = targetLevels.target_pwLong;
level_pwLong_jitter45 = targetLevels.target_pwLongJIT;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rng('shuffle');
period_pups=100; % period of power-up-pulses in microsec
nb_iterations=50;

level_pwShort_train=[level_pwShort_jitter0 level_pwShort_jitter45];
level_pwLong_train=[level_pwLong_jitter0 level_pwLong_jitter45];

level_pwShort=round(interp1([0 45.001],[level_pwShort_jitter0 level_pwShort_jitter45],jitter));
level_pwLong=round(interp1([0 45.001],[level_pwLong_jitter0 level_pwLong_jitter45],jitter));
    
% Generate Training stimuli
trainingFolder = sprintf('%sJitterExpt\\Stimuli\\Subject%s\\Training\\',folderpath,Subject);
if ~isfolder(trainingFolder)
    mkdir(trainingFolder);
    mkdir([trainingFolder '\pwShort']);
    mkdir([trainingFolder '\pwLong']);

end

for i=1:nb_iterations
    for j=1:length(jitter_train)
        
        outfile1=sprintf('%spwShort\\Subject%s_E%d_pw%d_jitter%d_level%d_Iter%d.xml',trainingFolder,Subject,ae,pwShort,round(jitter_train(j)),level_pwShort_train(j),i);
        genXMLstimulus_Jittered(device,processor,ae,re,pwShort,ipg,duration,period,jitter_train(j),period_pups,level_pwShort_train(j)-randi(rove_range,1),outfile1);

        outfile2=sprintf('%spwLong\\Subject%s_E%d_pw%d_jitter%d_level%d_Iter%d.xml',trainingFolder,Subject,ae,pwLong,round(jitter_train(j)),level_pwLong_train(j),i);
        genXMLstimulus_Jittered(device,processor,ae,re,pwLong,ipg,duration,period,jitter_train(j),period_pups,level_pwLong_train(j)-randi(rove_range_long,1),outfile2);

    end
end

% Generate Test stimuli
testingFolder = sprintf('%sJitterExpt\\Stimuli\\Subject%s\\Testing\\',folderpath,Subject);
if ~isfolder(testingFolder)
    mkdir(testingFolder);
    mkdir([testingFolder '\pwShort']);
    mkdir([testingFolder '\pwLong']);

end
for i=1:nb_iterations
    for j=1:length(jitter)
        
        outfile1=sprintf('%spwShort\\Subject%s_E%d_pw%d_jitter%d_level%d_Iter%d.xml',testingFolder,Subject,ae,pwShort,round(jitter(j)),level_pwShort(j),i);
        genXMLstimulus_Jittered(device,processor,ae,re,pwShort,ipg,duration,period,jitter(j),period_pups,level_pwShort(j)-randi(rove_range,1),outfile1);

        outfile2=sprintf('%spwLong\\Subject%s_E%d_pw%d_jitter%d_level%d_Iter%d.xml',testingFolder,Subject,ae,pwLong,round(jitter(j)),level_pwLong(j),i);
        genXMLstimulus_Jittered(device,processor,ae,re,pwLong,ipg,duration,period,jitter(j),period_pups,level_pwLong(j)-randi(rove_range_long,1),outfile2);

    end
end

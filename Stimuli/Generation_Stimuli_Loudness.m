rng('shuffle');
implant='CIC4';
processor='L-34';
ae=11; % active electrode
re=-3; % return electrode
jitter=[0 45]; % amount of jitter (expressed in % s.d.)
pw=[25 50 400]; % phase duration in microsec
level=0:255;
ipg=8; % Inter-phase gap in microsec
period=5000; % mean period in microsec
duration=400; % stim duration in ms
period_pups=100; % period of power-up-pulses in microsec

for i=1:length(pw),
    for j=1:length(jitter),
        for k=1:length(level),
            file=strcat('.\Loudness\',implant,'\E',num2str(ae),'_pw',num2str(pw(i)),'_jitter',num2str(jitter(j)),'_level',num2str(level(k)),'.xml');
            genXMLstimulus_Jittered(implant,processor,ae,re,pw(i),ipg,duration,period,jitter(j),period_pups,level(k),file);
        end
    end
end

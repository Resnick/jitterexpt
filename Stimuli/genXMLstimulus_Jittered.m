function genXMLstimulus(device,processor,ae,re,pw,ipg,duration,period,jitter,period_pups,level,outputFilename);

program='genXMLstimulus';

nb_pulses=round((duration*1000)/period)+1; % add one pulse so that the time between onset of first pulse and onset of last pulse is "exactly" equal to the total duration
intervals=period * (1 + (jitter/100)*randn(5*nb_pulses,1)); % generate 5 times more pulses than needed (should be enough to select periods within mean +/- 1 s.d.
all_valid_intervals=intervals(find( (intervals<=(period*(1+jitter/100))) & (intervals>=(period*(1-jitter/100))) )); % Limit the intervals to those < mean +/- 1 s.d.
valid_intervals=all_valid_intervals(1:nb_pulses-1);

valid_intervals_scaled=valid_intervals*(duration*1e3)/sum(valid_intervals); % scaled so that the onset of the ultimate pulse will occur 400 ms after the onset of the first pulse, independently from the phase duration value
valid_intervals_scaled=round([valid_intervals_scaled' period]); % add a last pulse with a "period" equal to the mean

for i=1:length(valid_intervals_scaled),
    nb_pups(i)=floor((valid_intervals_scaled(i)-(2*pw+ipg))/period_pups)-1; % Stick pups every 100 us
    intervals_final(i)=valid_intervals_scaled(i)-period_pups*nb_pups(i); % calculate new period for stimulus
end

% Print Info
fprintf(1, '%s: %s device, %s processor\n', program, device(1:4), processor(1:4)); 
fprintf(1, 'ae = \t\t');
fprintf(1, '%d\t', ae);
fprintf(1, '\nre = \t\t');
fprintf(1, '%d\t', re);
fprintf(1, '\nlevels = \t');
fprintf(1, '%d\t', level);
fprintf(1, '\npulse_width = %2.1f us\t interphase gap = %2.1f us\n', pw, ipg);

% Parameters Check

    
% Processor checks
if (processor == 'L-34'),
    if (pw > 434.4),
        fprintf(1, 'Phase duration cannot exceed 434.4 us with the L34\n');
        return;
    end
	if (ipg > 58),
        fprintf(1, 'Interphase gap cannot exceed 58 us with the L34\n');
		return;
    end
	if (~isempty(find(intervals_final > 13107)))
		fprintf(1, 'Period cannot exceed 13107 us with the L34\n');
		return;
    end
end

if (processor == 'SP12'),
    if (pw > 429.8),
        fprintf(1, 'Phase duration cannot exceed 429.8 us with the SP12\n');
        return;
    end
	if (ipg > 57),
        fprintf(1, 'Interphase gap cannot exceed 57 us with the SP12\n');
		return;
    end
	if (~isempty(find(intervals_final > 13078)))
		fprintf(1, 'Period cannot exceed 13078 us with the SP12\n');
		return;
    end
end

%Device checks
if strcmpi(device,'CIC3')
    if (pw < 25),
        fprintf(1, 'Phase duration cannot be less than 25 us with the CIC3\n');
        return;
    end
	if (ipg < 8),
        fprintf(1, 'Interphase gap cannot be less than 8 us with the CIC3\n');
		return;
    end
	if (~isempty(find(intervals_final < 70)))
		fprintf(1, 'Period cannot be less than 70 us with the CIC3\n');
		return;
    end
end

if strcmpi(device,'CIC4')
     if (~(pw == 9.6 && ipg == 4.8 && ~isempty(find(intervals_final >= 31.6))) && ...% Mode 1
		~(pw == 12 && ipg == 6 && ~isempty(find(intervals_final >= 38.8))) && ...% Mode 2
		~(pw == 14.4 && ipg >= 6.4 && ~isempty(find(intervals_final >= 51.8))) && ...% Mode 3
		~(pw == 18 && ipg >= 8 && ~isempty(find(intervals_final >= 64.6))) && ...% Mode 4
		~(pw >= 25 && ipg >= 7 && ~isempty(find(intervals_final >= 64.6)))) ...% Mode 5
        fprintf(1, 'Values of phase duration, IPG and/or period are not valid for stimulation with the CIC4\n');
		return;
     end
end

fid = fopen(outputFilename,'w');
if (fid == -1)
    fprintf(1, '%s could not be opened\n',outputFilename);
    return;
end

% XML header
fprintf(fid, '<nic:sequence xmlns:nic=''http://www.cochlear.com/nic''>\n');
% powerup sequences: 1200 pulses at 5000pps
fprintf(fid, '  <nic:sequence repeats=''1200''>\n');
fprintf(fid, '    <stimulus>\n');
fprintf(fid, '      <parameters><ae>0</ae><re>0</re><cl>20</cl><t>200.0</t></parameters>\n');
fprintf(fid, '    </stimulus>\n');
fprintf(fid, '  </nic:sequence>\n');

for i=1:length(intervals_final),
    
% stimulus parameters
    fprintf(fid, '  <nic:sequence repeats=''1''>\n');
    fprintf(fid, '    <stimulus>\n');
    fprintf(fid, '      <parameters><ae>%d</ae><re>%d</re><cl>%d</cl><pw>%2.1f</pw><pg>%2.1f</pg><t>%2.1f</t></parameters>\n',ae,re,level,pw,ipg,intervals_final(i));
    fprintf(fid, '    </stimulus>\n');
    fprintf(fid, '  </nic:sequence>\n');

% powerup sequences: pulses at 10000pps
    fprintf(fid, '  <nic:sequence repeats=''%d''>\n',nb_pups(i));
    fprintf(fid, '    <stimulus>\n');
    fprintf(fid, '      <parameters><ae>0</ae><re>0</re><cl>20</cl><t>100.0</t></parameters>\n');
    fprintf(fid, '    </stimulus>\n');
    fprintf(fid, '  </nic:sequence>\n');

end

fprintf(fid, '</nic:sequence>\n');
fclose(fid);

function [NOP,NOSC]=import_testing_file(SubjectInitials,Nb_Blocks,pwType,folderpath)
%[NOP,NOSC]=import_testing_file('XX',2,'pwLong','C:\\Users\\Macherey\\Documents\\CI testing\\JitterExpt\\Apex_Expt_Files\\SubjectXX\\Testing\\')

for i=1:Nb_Blocks,
    List_Files{i}=strcat('Subject',SubjectInitials,'_',pwType,'_Testing',num2str(i),'.cst');
end

List_Comparisons={'J7_','J10_','J15_','J21_','J31_','J45_'},

NOP(1:length(List_Comparisons))=0; % Number of presentations
NOSC(1:length(List_Comparisons))=0;  % Number of times signal was found

for j=1:length(List_Files),

    filename = strcat(folderpath,char(List_Files(j)));

    fid = fopen(filename,'r');
    C=textscan(fid,'%s','Delimiter','\t');
    fclose(fid);
    C = C{:};

    vec_start=find(~cellfun(@isempty, strfind(C,'[end]'))); % search for data after the [end] statement

    for k=1:length(List_Comparisons),
        ind=find(~cellfun(@isempty, strfind(C(vec_start:length(C)),char(List_Comparisons(k)))));
        for IDX=1:length(ind),
            NOP(k)=NOP(k)+str2double(C(vec_start+ind(IDX)));
            NOSC(k)=NOSC(k)+str2double(C(vec_start+ind(IDX)+1));   
        end
    end
end

jitter=[7 10 15 21 31 45];
figure;
semilogx(jitter,NOSC*100./NOP,'*-');
xlim([6 50]);
ylim([0 100]);
xlabel('Amount of jitter (%)');
ylabel('Percent correct');
title(pwType);


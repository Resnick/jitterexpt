function score=import_training_file(SubjectInitials,Nb_Blocks,pwType,folderpath)
%score=import_testing_file('XX',2,'pwLong','C:\\Users\\Macherey\\Documents\\CI testing\\JitterExpt\\Apex_Expt_Files\\SubjectXX\\Training\\')

NOP=0; % Number of presentations
NOSC=0; % Number of times signal was found

for i=1:Nb_Blocks,
    List_Files{i}=strcat('Subject',SubjectInitials,'_',pwType,'_Training',num2str(i),'.cst');
end   
 
for j=1:length(List_Files),

   filename = strcat(folderpath,char(List_Files(j)));

    fid = fopen(filename,'r');
    C=textscan(fid,'%s','Delimiter','\t');
    fclose(fid);
    C = C{:};

    vec_start=find(~cellfun(@isempty, strfind(C,'[end]'))); % search for data after the [end] statement

    ind=find(~cellfun(@isempty, strfind(C(vec_start:length(C)),'Iter')));
    for IDX=1:length(ind),
        NOP=NOP+str2double(C(vec_start+ind(IDX)));
        NOSC=NOSC+str2double(C(vec_start+ind(IDX)+1));   
    end
end

score=NOSC*100/NOP; % score in percent correct
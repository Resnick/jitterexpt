%% Subjects
SubjectInitials = {'S004L';'S0179JP'};
Nb_Blocks = [7,5];
addpath('.');

for subjIDX = 1:length(SubjectInitials)
    switch Nb_Blocks(subjIDX)
        case 5
            folderpath = sprintf('G:\\My Drive\\Lab\\CI Testing\\JitterExpt\\Apex_Expt_Files\\Subject%s\\Testing\\',SubjectInitials{subjIDX});
        case 7
            folderpath = sprintf('G:\\My Drive\\Lab\\CI Testing\\JitterExpt\\Apex_Expt_Files\\Subject%s\\Completed\\Testing\\',SubjectInitials{subjIDX});
    end
    pwType = 'pwLong';
    [longPossible(subjIDX,:),longCorrect(subjIDX,:)]=import_testing_file(SubjectInitials{subjIDX},Nb_Blocks(subjIDX),pwType,folderpath);
    pwType = 'pwShort';
    [shortPossible(subjIDX,:),shortCorrect(subjIDX,:)]=import_testing_file(SubjectInitials{subjIDX},Nb_Blocks(subjIDX),pwType,folderpath);
end

%% Plot
jitter=[7 10 15 21 31 45];

h = plot(jitter,shortCorrect./shortPossible,'-r.'); hold on;
set(h,'LineWidth',1.5,'MarkerSize',20)

h = plot(jitter,longCorrect./longPossible,'-b.'); hold on;
set(h,'LineWidth',1.5,'MarkerSize',20)
xlabel('Jitter (% of period)')
ylabel('Percent Correct')
set(gca,'Fontsize',14)
leghand = legend('25','400','Position','southeast');
set(leghand,'location','northwest')
title(leghand,{'Phase Duration','(\mus)'})
